package com.rptmat57.referral.service

import org.json.JSONException
import org.json.JSONObject
import java.io.File
import java.io.FileInputStream
import java.util.*


class ReferralService() {

    private var applicationInterface: ApplicationInterface? = null

    private val unitedStatesZipDistance = UnitedStatesZipDistance()

    private var costPerElement: Double = 0.005

    private val properties: Properties = Properties()

    private lateinit var apiKey: String

    private lateinit var errorFile: File

    private lateinit var patientList: List<Patient>

    private lateinit var mapperRegistry: MapperRegistry

    private var patientToDoc = LinkedHashMap<Patient, MutableMap<Doc, String?>>()

    var cost: Double = 0.0

    var hasError: Boolean = false

    constructor(applicationInterface: ApplicationInterface?, filterAsCrowFlies: Double?, calculateCrowOnly: Boolean) : this() {
        try {
            this.applicationInterface = applicationInterface

            properties.load(FileInputStream("config.properties"))
            costPerElement = properties.getProperty("google_maps_cost_per_element", "0.005").toDouble()
            errorFile = File(properties.getProperty("output_error_file_path"))
            errorFile.delete()

            if (!calculateCrowOnly) {
                getGoogleAPIKey()
            }

            mapperRegistry = MapperRegistry(properties)

            val docFile = properties.getProperty("docs_file_path")
            val patientFile = properties.getProperty("patients_file_path")
            val subFile = properties.getProperty("substitutions_file_path")
            val docList = mapperRegistry.getDocProcessor(docFile).map(docFile)
            patientList = mapperRegistry.getPatientProcessor(patientFile).map(patientFile)
            val specialtyMap = mapperRegistry.getSubstitutionProcessor(subFile).map(subFile)

            var totalElements = 0

            // First loop to gather data and evaluate how many elements to request for Google Maps API
            for (patient in patientList) {
                val potentialSubstitutionList = specialtyMap[patient.specialtySought] ?: listOf(patient.specialtySought)
                val potentialDocList = docList.asSequence().filter { potentialSubstitutionList.contains(it.specialty) }.map { it to null as String? }.toMap().toMutableMap()
                if (filterAsCrowFlies != null || calculateCrowOnly) {
                    val mutableIterator = potentialDocList.iterator()
                    for (e in mutableIterator) {
                        val patientZip = patient.zipOnly5Digits()
                        val docZip = e.key.zipOnly5Digits()
                        if (docZip != null && patientZip != null) {
                            val distance = unitedStatesZipDistance.calculateDistanceBetweenZipsInMiles(patientZip, docZip)
                            if (calculateCrowOnly && distance != null) {
                                e.setValue(String.format("%.2f", distance))
                            }
                            if (filterAsCrowFlies != null && distance != null && distance > filterAsCrowFlies) {
                                mutableIterator.remove()
                            }
                        }
                    }
                    if (calculateCrowOnly) {
                        applicationInterface?.setProgress(patientList.indexOf(patient) * .95 / patientList.size)
                    }
                }
                patientToDoc[patient] = potentialDocList
                totalElements += potentialDocList.size
            }

            if (calculateCrowOnly) {
                writeResultsToFile()
            } else {
                cost = totalElements * costPerElement
            }
        } catch (e: Exception) {
            hasError = true
            writeError(e)
        }
    }

    private fun getGoogleAPIKey() {
        val apiKeyFile = File(properties.getProperty("api_key_file_path"))
        apiKey = ""
        if (apiKeyFile.exists()) {
            val lines = apiKeyFile.readLines()
            if (lines.isNotEmpty()) apiKey = lines.first()
        }

        if (apiKey.isBlank()) {
            val apiError = "No API KEY found. Successful requests are not guaranteed without an API key"
            applicationInterface?.setMessage(apiError)
        }
    }

    fun startProcessing() {
        try {
            for (patient in patientList) {
                val entry = patientToDoc[patient]
                if (entry != null && entry.isNotEmpty()) {
                    requestDistancesFromGoogleAPI(patient, entry, apiKey)
                }
                applicationInterface?.setProgress(patientList.indexOf(patient) * .95 / patientList.size)
            }

            writeResultsToFile()
        } catch (e: Exception) {
            hasError = true
            writeError(e)
        }
    }

    private fun writeResultsToFile() {
        mapperRegistry.getOutputProcessor(properties.getProperty("output_file_path")).map(patientToDoc)
        applicationInterface?.setProgress(1.0)
        applicationInterface?.setMessage("Done!")
        applicationInterface?.reset()
    }

    private fun requestDistancesFromGoogleAPI(patient: Patient, docList: MutableMap<Doc, String?>, apiKey: String) {
        val destinations = docList.keys.joinToString("|") { it.zipForRequest() }
        val params = mutableMapOf("origins" to patient.zipForRequest(), "destinations" to destinations)
        if (apiKey.isNotBlank()) {
            params["key"] = apiKey
        }
        val distances = khttp.get(url = "https://maps.googleapis.com/maps/api/distancematrix/json", params = params).jsonObject
        val rows = distances.optJSONArray("rows")?.optJSONObject(0)?.optJSONArray("elements")?.toList()
        if (rows != null) {
            for (doc in docList.keys) {
                val i = docList.keys.indexOf(doc)
                val distance = (rows[i] as JSONObject).optJSONObject("distance")?.get("value")?.toString()?.toDouble()?.times(0.00062137)
                when {
                    distance != null -> docList[doc] = String.format("%.2f", distance)
                    (rows[i] as JSONObject).get("status") == "NOT_FOUND" -> docList[doc] = "Address not found"
                    (rows[i] as JSONObject).get("status") == "ZERO_RESULTS" -> docList[doc] = "No route could be find between patient and doctor"
                    (rows[i] as JSONObject).get("status") == "MAX_ROUTE_LENGTH_EXCEEDED" -> docList[doc] = "Requested route is too long and cannot be processed"
                    else -> docList[doc] = "Error"
                }
            }
        } else {
            try {
                if (distances.get("error_message") != null) {
                    throw Exception(distances.get("error_message")?.toString())
                }
            } catch (e: JSONException) {
                try {
                    if (distances.get("status") != null) {
                        throw Exception(params.toString() + ": " + distances.get("status")?.toString())
                    }
                } catch (e: JSONException) {
                    throw Exception(e.toString())
                }
            }
        }
    }

    private fun writeError(error: Exception?) {
        if (error != null) {
            applicationInterface?.setError(error)
            System.out.println(error)
            errorFile.appendText(error.toString())
            errorFile.appendText(System.getProperty("line.separator"))
        }
    }

    class Doc(val zip: String, val specialty: String?, val original: Map<String, String?>) {
        override fun toString(): String {
            return "com.Doc(zip=$zip, specialty='$specialty')"
        }

        fun zipOnly5Digits(): String? {
            return if (zip.length >= 5) {
                zip.substring(0, 5)
            } else {
                null
            }
        }

        fun zipForRequest(): String {
            return if (zip.chars().allMatch(Character::isDigit)) {
                "$zip zip"
            } else {
                zip
            }
        }
    }

    class Patient(val zip: String, val specialtySought: String?, val original: Map<String, String?>) {
        override fun toString(): String {
            return "com.Patient(zip=$zip, specialtySought='$specialtySought')"
        }

        fun zipOnly5Digits(): String? {
            return if (zip.length >= 5) {
                zip.substring(0, 5)
            } else {
                null
            }
        }

        fun zipForRequest(): String {
            return if (zip.chars().allMatch(Character::isDigit)) {
                "$zip zip"
            } else {
                zip
            }
        }
    }
}