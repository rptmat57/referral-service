package com.rptmat57.referral.service

import javafx.application.Application
import javafx.application.Platform
import javafx.fxml.FXML
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.scene.control.*
import javafx.scene.control.Alert.AlertType
import javafx.scene.layout.GridPane
import javafx.scene.layout.Priority
import javafx.stage.Stage
import java.io.PrintWriter
import java.io.StringWriter
import kotlin.concurrent.thread


fun main(args: Array<String>) {
    Application.launch(ApplicationMain::class.java, *args)
}

interface ApplicationInterface {
    fun setProgress(progress: Double)

    fun setError(exception: java.lang.Exception)

    fun setMessage(message: String)

    fun reset()
}

class ApplicationMain : ApplicationInterface, Application() {

    @FXML
    private lateinit var startButton: Button

    @FXML
    private lateinit var stopButton: Button

    @FXML
    private lateinit var progressBar: ProgressBar

    @FXML
    private lateinit var errorLabel: Label

    @FXML
    private lateinit var titleLabel: Label

    @FXML
    private lateinit var label: Label

    @FXML
    private lateinit var filterBox: CheckBox

    @FXML
    private lateinit var filterBoxCrowOnly: CheckBox

    @FXML
    private lateinit var filterInput: TextField

    override fun start(primaryStage: Stage) {
        try {
            primaryStage.title = "Referral Service"
            val root = FXMLLoader.load<Parent>(javaClass.classLoader.getResource("layout.fxml"))
            val scene1 = Scene(root)
            primaryStage.scene = scene1
            primaryStage.show()
        } catch (exception: Exception) {
            System.out.println(exception.message)
            throw exception
        }
    }

    override fun stop() {
        Platform.exit()
        System.exit(0)
    }

    @FXML
    fun startProcessing() {
        val filter = filterBox.isSelected
        val distanceToFilter = if (filter) filterInput.text.toDoubleOrNull() else null

        if (filterBoxCrowOnly.isSelected) {
            thread(start = true) {
                ReferralService(this, distanceToFilter, filterBoxCrowOnly.isSelected)
            }
            displayProcessing()
        } else {
            val referralService = ReferralService(this, distanceToFilter, filterBoxCrowOnly.isSelected)
            if (!referralService.hasError && !filterBoxCrowOnly.isSelected) {
                val alert = Alert(AlertType.CONFIRMATION)
                alert.title = "Confirmation Dialog"
                alert.headerText = "Processing this request through Google Maps will cost an estimated $${referralService.cost}"
                alert.contentText = "Are you ok with this?"

                val result = alert.showAndWait()
                if (result.get() === ButtonType.OK) {
                    thread(start = true) {
                        referralService.startProcessing()
                    }
                    displayProcessing()
                } else {
                    // ... user chose CANCEL or closed the dialog
                }
            }
        }
    }

    private fun displayProcessing() {
        Platform.runLater {
            errorLabel.text = ""
            startButton.isDisable = true
            stopButton.isDisable = false
        }
    }

    @FXML
    fun stopProcessing() {
        this.stop()
    }

    override fun setProgress(progress: Double) {
        Platform.runLater { progressBar.progress = progress }
    }

    override fun setMessage(message: String) {
        Platform.runLater { errorLabel.text = message }
    }

    override fun setError(exception: java.lang.Exception) {
        Platform.runLater {
            setMessage(exception.message!!)
            val alert = Alert(AlertType.ERROR)
            alert.title = "Exception Dialog"
            alert.headerText = "An error occurred"
            alert.contentText = exception.message

            // Create expandable Exception.
            val sw = StringWriter()
            val pw = PrintWriter(sw)
            exception.printStackTrace(pw)
            val exceptionText = sw.toString()

            val label = Label("The exception stacktrace was:")

            val textArea = TextArea(exceptionText)
            textArea.isEditable = false
            textArea.isWrapText = true

            textArea.maxWidth = java.lang.Double.MAX_VALUE
            textArea.maxHeight = java.lang.Double.MAX_VALUE
            GridPane.setVgrow(textArea, Priority.ALWAYS)
            GridPane.setHgrow(textArea, Priority.ALWAYS)

            val expContent = GridPane()
            expContent.maxWidth = java.lang.Double.MAX_VALUE
            expContent.add(label, 0, 0)
            expContent.add(textArea, 0, 1)

            // Set expandable Exception into the dialog pane.
            alert.dialogPane.expandableContent = expContent

            alert.showAndWait()
        }
    }

    override fun reset() {
        startButton.isDisable = false
        stopButton.isDisable = true
    }
}