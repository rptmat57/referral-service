package com.rptmat57.referral.service

import com.rptmat57.referral.service.ReferralService.Doc
import com.rptmat57.referral.service.ReferralService.Patient
import org.apache.poi.ss.usermodel.DataFormatter
import org.apache.poi.ss.usermodel.FillPatternType
import org.apache.poi.ss.usermodel.IndexedColors
import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.util.*

class MapperRegistry(properties: Properties) {
    private val defaultExtension = "csv"
    private val patientRegistry = mapOf(defaultExtension to CsvPatientsProcessor(properties), "xlsx" to XlsxPatientsProcessor(properties))
    private val docRegistry = mapOf(defaultExtension to CsvDocsProcessor(properties), "xlsx" to XlsxDocsProcessor(properties))
    private val outputRegistry = mapOf(defaultExtension to CsvOutputProcessor(properties), "xlsx" to XlsxOutputProcessor(properties))
    private val substitutionRegistry = mapOf(defaultExtension to CsvSubstitutionsProcessor(properties))

    fun getPatientProcessor(fileName: String): Mapper<String, List<Patient>> {
        return patientRegistry[fileName.substringAfterLast(".")] ?: patientRegistry[defaultExtension]!!
    }

    fun getDocProcessor(fileName: String): Mapper<String, List<Doc>> {
        return docRegistry[fileName.substringAfterLast(".")] ?: docRegistry[defaultExtension]!!
    }

    fun getSubstitutionProcessor(fileName: String): Mapper<String, Map<String, List<String>>> {
        return substitutionRegistry[fileName.substringAfterLast(".")] ?: substitutionRegistry[defaultExtension]!!
    }

    fun getOutputProcessor(fileName: String): Mapper<Map<Patient, Map<Doc, String?>>, Any> {
        return outputRegistry[fileName.substringAfterLast(".")] ?: outputRegistry[defaultExtension]!!
    }
}

abstract class Mapper<From, To>(val properties: Properties) {
    abstract fun map(from: From): To
}

class CsvPatientsProcessor(properties: Properties) : Mapper<String, List<Patient>>(properties) {

    override fun map(from: String): List<Patient> {
        val patientList = mutableListOf<Patient>()
        val file = File(from)
        for (item in file.readLines()) {
            val splitEntry = item.split(delimiterNotInQuotes(properties.getProperty("delimiter", ",")))
            patientList.add(Patient(splitEntry[1], splitEntry[2], mapOf("line" to item)))
        }

        return patientList
    }
}

class CsvDocsProcessor(properties: Properties) : Mapper<String, List<Doc>>(properties) {
    override fun map(from: String): List<Doc> {
        val docList = mutableListOf<Doc>()
        val file = File(from)
        for (item in file.readLines()) {
            val splitEntry = item.split(delimiterNotInQuotes(properties.getProperty("delimiter", ",")))
            docList.add(Doc(splitEntry[1], splitEntry[2], mapOf("line" to item)))
        }

        return docList
    }
}

class CsvSubstitutionsProcessor(properties: Properties) : Mapper<String, Map<String, List<String>>>(properties) {
    override fun map(from: String): Map<String, List<String>> {
        val substitutionMap = mutableMapOf<String, List<String>>()
        val file = File(from)
        for (item in file.readLines()) {
            val splitEntry = item.split(delimiterNotInQuotes(properties.getProperty("delimiter", ",")))
            val substitutionList = mutableListOf<String>()
            for (entry in splitEntry) {
                substitutionList.add(processEntry(entry))
            }
            substitutionMap[processEntry(splitEntry[0])] = substitutionList
        }
        return substitutionMap
    }
}

class XlsxPatientsProcessor(properties: Properties) : Mapper<String, List<Patient>>(properties) {
    override fun map(from: String): List<Patient> {
        val dataFormatter = DataFormatter()
        val patientList = mutableListOf<Patient>()
        val file = FileInputStream(File(from))
        val workbook = XSSFWorkbook(file)
        val headerRowIndex = if (properties.getProperty("xlsx_patient_header_row") != null) properties.getProperty("xlsx_patient_header_row").toInt() else 0
        val sheet = workbook.getSheetAt(0)
        val rows = sheet.toList()
        if (rows.isNotEmpty()) {
            val headerRow = sheet.getRow(headerRowIndex).toList()
            for (i in headerRowIndex + 1 until rows.size) {
                val original = mutableMapOf<String, String?>()
                for (j in headerRow.indices) {
                    original[headerRow[j].stringCellValue] = dataFormatter.formatCellValue(rows[i].getCell(j))?.trim()
                }
                val zip = original[dataFormatter.formatCellValue(headerRow[properties.getProperty("xlsx_patient_zip_column").toInt()])]
                val specialty = original[dataFormatter.formatCellValue(headerRow[properties.getProperty("xlsx_patient_specialty_column").toInt()])]
                if (zip != null && zip.isNotBlank()) {
                    patientList.add(Patient(zip, specialty, original.toMap()))
                }
            }
        }

        workbook.close()
        file.close()
        return patientList
    }

}

class XlsxDocsProcessor(properties: Properties) : Mapper<String, List<Doc>>(properties) {
    override fun map(from: String): List<Doc> {
        val dataFormatter = DataFormatter()
        val docList = mutableListOf<Doc>()
        val file = FileInputStream(File(from))
        val workbook = XSSFWorkbook(file)
        val headerRowIndex = if (properties.getProperty("xlsx_doc_header_row") != null) properties.getProperty("xlsx_doc_header_row").toInt() else 0
        val sheet = workbook.getSheetAt(0)
        val rows = sheet.toList()
        if (rows.isNotEmpty()) {
            val headerRow = sheet.getRow(headerRowIndex).toList()
            for (i in headerRowIndex + 1 until rows.size) {
                val original = mutableMapOf<String, String?>()
                for (j in headerRow.indices) {
                    original[dataFormatter.formatCellValue(headerRow[j])] = dataFormatter.formatCellValue(rows[i].getCell(j))?.trim()
                }
                val zip = original[dataFormatter.formatCellValue(headerRow[properties.getProperty("xlsx_doc_zip_column").toInt()])]
                val specialty = original[dataFormatter.formatCellValue(headerRow[properties.getProperty("xlsx_doc_specialty_column").toInt()])]
                if (zip != null && zip.isNotBlank()) {
                    docList.add(Doc(zip, specialty, original.toMap()))
                }
            }
        }

        workbook.close()
        file.close()
        return docList
    }

}

class CsvOutputProcessor(properties: Properties) : Mapper<Map<Patient, Map<Doc, String?>>, Any>(properties) {
    override fun map(from: Map<Patient, Map<Doc, String?>>): Any {
        val delimiter = properties.getProperty("delimiter", ",")
        val outputFile = File(properties.getProperty("output_file_path"))
        outputFile.bufferedWriter().use { out ->
            from.forEach { entry ->
                out.write(delimiter + "Type" + delimiter + "Zip" + delimiter + "Specialty")
                entry.key.original.forEach {
                    out.write(delimiter + "\"" + it.key + "\"")
                }
                out.newLine()
                out.write(delimiter + entry.key.javaClass.simpleName + delimiter + entry.key.zip + delimiter + "\"" + entry.key.specialtySought + "\"")
                entry.key.original.forEach {
                    out.write(delimiter + "\"" + it.value + "\"")
                }
                out.newLine()
                out.write("Distance (miles)" + delimiter + "Type" + delimiter + "Zip" + delimiter + "Specialty")
                val matchOriginal = entry.value.keys.iterator().next()
                matchOriginal.original.forEach {
                    out.write(delimiter + "\"" + it.key + "\"")
                }
                out.newLine()
                entry.value.entries.sortedWith(compareBy(nullsLast()) { it: Map.Entry<Doc, String?> ->
                    return@compareBy try {
                        it.value?.toDouble()
                    } catch (e: java.lang.Exception) {
                        null
                    }
                }).forEach { pair ->
                    out.write(pair.value);out.write(delimiter + pair.key.javaClass.simpleName + delimiter + pair.key.zip + delimiter + "\"" + pair.key.specialty + "\"");pair.key.original.forEach {
                    out.write(delimiter + "\"" + it.value + "\"")
                };out.newLine()
                }
                out.newLine()
            }
        }
        return outputFile
    }
}

class XlsxOutputProcessor(properties: Properties) : Mapper<Map<Patient, Map<Doc, String?>>, Any>(properties) {
    override fun map(from: Map<Patient, Map<Doc, String?>>): Any {
        val workbook = XSSFWorkbook()
        //val createHelper = workbook.creationHelper

        val sheet = workbook.createSheet("Results")

        // Font and Style
        val headerFont = workbook.createFont()
        headerFont.bold = true
        headerFont.color = IndexedColors.WHITE.getIndex()
        val headerCellStyle = workbook.createCellStyle()
        headerCellStyle.setFont(headerFont)
        headerCellStyle.fillForegroundColor = IndexedColors.BLUE_GREY.index
        headerCellStyle.fillPattern = FillPatternType.SOLID_FOREGROUND

        val patientHeadercolumns = listOf("Type", "Zip", "Specialty")
        val docHeadercolumns = listOf("Distance (miles)", "Type", "Zip", "Specialty")

        var rowIndex = 0
        from.forEach { entry ->
            val patientHeaderRow = sheet.createRow(rowIndex++)
            for (col in patientHeadercolumns.indices) {
                val cell = patientHeaderRow.createCell(col + 1)
                cell.setCellValue(patientHeadercolumns[col])
                cell.cellStyle = headerCellStyle
            }
            entry.key.original.forEach {
                val cell = patientHeaderRow.createCell(entry.key.original.entries.indexOf(it) + patientHeadercolumns.size + 1)
                cell.setCellValue(it.key)
                cell.cellStyle = headerCellStyle
            }
            val patientRow = sheet.createRow(rowIndex++)
            var patientCellIndex = 1
            patientRow.createCell(patientCellIndex++).setCellValue(entry.key.javaClass.simpleName)
            patientRow.createCell(patientCellIndex++).setCellValue(entry.key.zip)
            patientRow.createCell(patientCellIndex++).setCellValue(entry.key.specialtySought)
            entry.key.original.forEach {
                patientRow.createCell(entry.key.original.entries.indexOf(it) + patientCellIndex).setCellValue(it.value)
            }
            val match = entry.value.keys
            if (match.isNotEmpty()) {
                val docHeaderRow = sheet.createRow(rowIndex++)
                for (col in docHeadercolumns.indices) {
                    val cell = docHeaderRow.createCell(col)
                    cell.setCellValue(docHeadercolumns[col])
                    cell.cellStyle = headerCellStyle
                }
                val matchOriginal = entry.value.keys.iterator().next()
                matchOriginal.original.forEach {
                    val cell = docHeaderRow.createCell(matchOriginal.original.entries.indexOf(it) + docHeadercolumns.size)
                    cell.setCellValue(it.key)
                    cell.cellStyle = headerCellStyle
                }
            } else {
                val docHeaderRow = sheet.createRow(rowIndex++)
                docHeaderRow.createCell(0).setCellValue("No Matches")
            }
            entry.value.entries.sortedWith(compareBy(nullsLast()) { it: Map.Entry<Doc, String?> ->
                return@compareBy try {
                    it.value?.toDouble()
                } catch (e: java.lang.Exception) {
                    null
                }
            }).forEach { pair ->
                val docRow = sheet.createRow(rowIndex++)
                var cellIndex = 0
                docRow.createCell(cellIndex++).setCellValue(pair.value)
                docRow.createCell(cellIndex++).setCellValue(pair.key.javaClass.simpleName)
                docRow.createCell(cellIndex++).setCellValue(pair.key.zip)
                docRow.createCell(cellIndex++).setCellValue(pair.key.specialty)
                pair.key.original.forEach {
                    docRow.createCell(pair.key.original.entries.indexOf(it) + cellIndex).setCellValue(it.value)
                }
            }
            rowIndex++
        }

        autoSizeSheet(sheet, null)

        val fileOut = FileOutputStream(properties.getProperty("output_file_path"))
        workbook.write(fileOut)
        fileOut.close()
        workbook.close()
        return Any()
    }
}

fun autoSizeSheet(sheet: XSSFSheet, rowHeightInPoints: Float?) {
    // auto-size
    if (sheet.physicalNumberOfRows != 0) {
        var columnNumber = 0
        for (i in sheet.firstRowNum..sheet.lastRowNum) {
            if (sheet.getRow(i) == null) {
                sheet.createRow(i)
            }
            columnNumber = Math.max(columnNumber, sheet.getRow(i).lastCellNum.toInt())
            if (rowHeightInPoints != null) {
                sheet.getRow(i).heightInPoints = rowHeightInPoints
            }
        }
        for (i in 0..columnNumber) {
            sheet.autoSizeColumn(i)
        }
    }
}

fun delimiterNotInQuotes(delimiter: String): Regex {
    return "$delimiter(?=(?:[^\\\"]*\\\"[^\\\"]*\\\")*[^\\\"]*\$)".toRegex()
}

fun processEntry(entry: String): String {
    var treatedEntry = entry
    if (treatedEntry.startsWith("\"")) treatedEntry = treatedEntry.substring(1)
    if (treatedEntry.endsWith("\"")) treatedEntry = treatedEntry.substring(0, treatedEntry.length - 1)
    return treatedEntry.trim()
}