package com.rptmat57.referral.service

import java.io.BufferedReader
import java.io.InputStreamReader

class UnitedStatesZipDistance {

    private val zipToLatLong: MutableMap<String, Pair<Double, Double>> = mutableMapOf()

    init {
        val zipFile = BufferedReader(InputStreamReader(javaClass.classLoader.getResourceAsStream("2018_zip_lat_long.txt")))
        for (item in zipFile.readLines()) {
            val c = item.toCharArray()[0]
            if (c !in 'A'..'Z') {
                val splitEntry = item.split(delimiterNotInQuotes("\t"))
                zipToLatLong[splitEntry[0]] = Pair(splitEntry[5].toDouble(), splitEntry[6].toDouble())
            }
        }
    }

    fun calculateDistanceBetweenZipsInMiles(zip1: String, zip2: String): Double? {
        var distance = null as Double?
        if (zipToLatLong[zip1] != null && zipToLatLong[zip2] != null) {
            distance = DistanceCalculator.instance.distanceInMiles(zipToLatLong[zip1]?.first!!, zipToLatLong[zip1]?.second!!, zipToLatLong[zip2]?.first!!, zipToLatLong[zip2]?.second!!)
        }
        return distance
    }
}

class DistanceCalculator private constructor() {

    companion object {

        private const val radiusKms: Long = 6371 // for calculation in kilometers
        private const val radiusMiles: Long = 3959 // for calculation in miles

        val instance = DistanceCalculator()
    }

    fun distanceInKms(fromLatitude: Double, fromLongitude: Double,
                      toLatitude: Double, toLongitude: Double): Double {
        return distanceFromLatLon(fromLatitude, fromLongitude, toLatitude, toLongitude, Unit.KMS)
    }

    fun distanceInMiles(fromLatitude: Double, fromLongitude: Double,
                        toLatitude: Double, toLongitude: Double): Double {
        return distanceFromLatLon(fromLatitude, fromLongitude, toLatitude, toLongitude, Unit.MILES)
    }

    private fun distanceFromLatLon(lat1: Double, lon1: Double, lat2: Double, lon2: Double, unit: Unit): Double {
        val radius = getRadius(unit) // Radius of the earth in unit
        val dLat = deg2rad(lat2 - lat1)
        val dLon = deg2rad(lon2 - lon1)
        val a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
                Math.sin(dLon / 2) * Math.sin(dLon / 2)
        return radius * 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
    }

    private fun getRadius(unit: Unit): Long {
        return when (unit) {
            Unit.MILES -> radiusMiles
            else -> radiusKms
        }
    }

    private fun deg2rad(deg: Double): Double {
        return deg * (Math.PI / 180)
    }

}

enum class Unit {
    KMS, MILES
}