package com.rptmat57.referral.service

import org.testng.Assert
import org.testng.annotations.Test
import java.io.File

class ReferralServiceTest {
    @Test
    fun testReferralService() {
        System.out.println(System.getProperty("user.dir"))
        ReferralService(null, null, true).startProcessing()
        Assert.assertTrue(File("results.csv").exists())
    }
}