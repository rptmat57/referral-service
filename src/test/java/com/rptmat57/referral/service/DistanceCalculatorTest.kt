package com.rptmat57.referral.service

import org.testng.Assert
import org.testng.annotations.Test

class DistanceCalculatorTest {
    @Test
    fun testDistance() {
        val lat1 = 33.057039
        val long1 = -80.182161

        val lat2 = 39.150349
        val long2 = -77.078251

        val approximateDistanceMiles = 455
        val approximateDistanceKms = 732

        val distanceMiles = DistanceCalculator.instance.distanceInMiles(lat1, long1, lat2, long2)
        Assert.assertTrue(distanceMiles.minus(approximateDistanceMiles) < 1)
        Assert.assertTrue(distanceMiles.minus(approximateDistanceMiles) > -1)

        val distanceKms = DistanceCalculator.instance.distanceInKms(lat1, long1, lat2, long2)
        Assert.assertTrue(distanceKms.minus(approximateDistanceKms) < 5)
        Assert.assertTrue(distanceKms.minus(approximateDistanceKms) > -5)
    }
}